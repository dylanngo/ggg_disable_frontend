<?php

declare(strict_types=1);

namespace GGG\DisableFrontend\Observer;

use Magento\Framework\App\Action\Action;

/**
 * Class DisableFrontend
 */
class DisableFrontend implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    private $actionFlag;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    private $redirect;

    /**
     * @var \GGG\DisableFrontend\Helper\Data
     */
    private $data;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * DisableFrontend constructor.
     *
     * @param \Magento\Framework\App\ActionFlag $actionFlag
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \GGG\DisableFrontend\Helper\Data $data
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \GGG\DisableFrontend\Helper\Data $data,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->actionFlag = $actionFlag;
        $this->redirect = $redirect;
        $this->data = $data;
        $this->request = $request;
    }

    /**
     * Execute.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->data->isEnabled() && $this->data->isRouteDisabled($this->request->getRouteName())) {
            $this->actionFlag->set('', Action::FLAG_NO_DISPATCH, 'true');
            $controller = $observer->getControllerAction();
            $this->redirect->redirect($controller->getResponse(), $this->data->getEndpointUrl());
        }
    }
}
