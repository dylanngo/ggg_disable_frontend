<?php

declare(strict_types=1);

namespace GGG\DisableFrontend\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 */
class Data extends AbstractHelper
{
    const XML_CONFIG_PATH = 'disable_frontend/';

    /**
     * Is Enable.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_CONFIG_PATH . 'general/enable') == '1';
    }

    /**
     * Get End Point Url.
     *
     * @return string
     */
    public function getEndpointUrl()
    {
        return $this->scopeConfig->getValue(self::XML_CONFIG_PATH . 'general/endpoint_url');
    }

    /**
     * Get Route Disabled List.
     *
     * @return array
     */
    public function getRouteDisabledList()
    {
        $value = $this->scopeConfig->getValue('disable_frontend/general/route_disabled');
        $routes = preg_split("/\r\n|\n|\r/", $value) ?: [];
        return array_filter($routes);
    }

    /**
     * Is Route Disabled.
     *
     * @param string|null $routeName
     * @return bool
     */
    public function isRouteDisabled(?string $routeName): bool
    {
        return in_array($routeName, $this->getRouteDisabledList());
    }
}
