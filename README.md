Module support disable frontend

# Installation
	* composer config repositories.ggg-disable-frontend git git@gitlab.com:dylanngo/disable-frontend.git
	* composer require ggg/disable-frontend:master
	* php bin/magento module:enable GGG_DisableFrontend

## Changelog
* 1.0.1 Redirect to frontend url
* 1.0.0 Init
